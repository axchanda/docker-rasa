#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive
apt update -y
apt upgrade -y
apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common nano -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu
$(lsb_release -cs)
stable"
apt-get update -y
apt-get install docker-ce docker-ce-cli containerd.io -y
systemctl start docker
usermod ubuntu -G docker
echo 'e040d96399e885c31344a1289572e84a56e962f3' > TOKEN.txt
cat TOKEN.txt | docker login https://docker.pkg.github.com -u axchanda --password-stdin
rm -rf TOKEN.txt