export DEBIAN_FRONTEND=noninteractive
apt update -y
apt upgrade -y
apt install software-properties-common -y
add-apt-repository ppa:deadsnakes/ppa -y
apt update -y
apt install apt-utils -y
apt install python3.6 python3-dev git curl make -y
curl https://bootstrap.pypa.io/get-pip.py -o /usr/src/get-pip.py
ln -s /usr/bin/python3 /usr/bin/python
python3 /usr/src/get-pip.py
pip3 install 'rasa_nlu~=0.15.0' flask
apt install binutils -y
git clone https://github.com/aws/efs-utils /usr/src/efs-utils
cd /usr/src/efs-utils
sh ./build-deb.sh
apt install ./build/amazon-efs-utils*deb -y
echo "fs-575943d4:/ /home/ubuntu/models/ efs _netdev 0 0" >> /etc/fstab