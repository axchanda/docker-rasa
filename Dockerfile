FROM ubuntu:bionic
RUN apt-get update -y 
RUN apt-get install apt-utils software-properties-common -y
RUN add-apt-repository ppa:deadsnakes/ppa -y && apt update -y
RUN apt-get upgrade -y
RUN apt-get install gcc g++ python3.6 python3-dev git curl make -y 
RUN curl https://bootstrap.pypa.io/get-pip.py -o /usr/src/get-pip.py
RUN ln -s /usr/bin/python3 /usr/bin/python 
RUN python3 /usr/src/get-pip.py
ADD requirements.txt /usr/src/requirements.txt
RUN pip3 install -r /usr/src/requirements.txt